{
  const {
    html,
  } = Polymer;
  /**
    `<multiple-icons>` Description.

    Example:

    ```html
    <multiple-icons></multiple-icons>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --multiple-icons | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class MultipleIcons extends Polymer.Element {

    static get is() {
      return 'multiple-icons';
    }

    static get properties() {
      return {
        description: String,
        icons: {
          type: Array,
          value: function() {
            return [];
          }
        }
      };
    }


  }

  customElements.define(MultipleIcons.is, MultipleIcons);
}